package org.yah.tools.neuralnetwork.custom;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Iterator;

import org.junit.Test;

public class LabeledResultsTest {

	@Test
	public void testIterator() {
		LabeledResults<String> res = new LabeledResults<>(new float[][] { { 1, 3, 2 }, { 4, 1, 5 }, { 5, 1, 5 } },
				new String[] { "A", "B", "C" });
		Iterator<String> iterator = res.iterator();
		assertTrue(iterator.hasNext());
		assertEquals("B", iterator.next());

		assertTrue(iterator.hasNext());
		assertEquals("C", iterator.next());

		assertTrue(iterator.hasNext());
		assertEquals("A", iterator.next());
	}

}
