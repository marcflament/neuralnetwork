package org.yah.tools.neuralnetwork.custom;

import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;

public class NDArraySandbox {

	public NDArraySandbox() {
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		INDArray zeros = Nd4j.zeros(3, 2);
		INDArray ones = Nd4j.ones(3, 2);
		zeros.assign(ones);
		System.out.println(zeros);
		System.out.println(ones);
	}
}
