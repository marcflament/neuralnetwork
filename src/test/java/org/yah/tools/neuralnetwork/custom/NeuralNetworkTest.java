package org.yah.tools.neuralnetwork.custom;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertThat;

import java.io.IOException;
import java.util.Objects;

import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.junit.Test;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;
import org.yah.tools.neuralnetwork.custom.NeuralNetwork.Layer;
import org.yah.tools.neuralnetwork.custom.json.INDArrayModule;

import com.fasterxml.jackson.databind.ObjectMapper;

public class NeuralNetworkTest {

	@Test
	public void testInputsCount() {
		NeuralNetwork network = NeuralNetwork.build(2).withLayer(2).add().build();
		assertEquals(2, network.inputsCount());

		network = NeuralNetwork.build(1).withLayer(2).add().build();
		assertEquals(1, network.inputsCount());
	}

	@Test
	public void testOutputsCount() {
		NeuralNetwork network = NeuralNetwork.build(2).withLayer(2).add().build();
		assertEquals(2, network.outputsCount());
		network = NeuralNetwork.build(10).withLayer(5).add().withLayer(2).add().build();
		assertEquals(2, network.outputsCount());
	}

	@Test
	public void testLayers() {
		NeuralNetwork network = NeuralNetwork.build(2).withLayer(2).add().build();
		assertArrayEquals(new int[] { 2, 2 }, network.layers());

		network = NeuralNetwork.build(10).withLayer(5).add().withLayer(2).add().build();
		assertArrayEquals(new int[] { 10, 5, 2 }, network.layers());
	}

	@Test
	public void testEvaluate() {
		ActivationFunction activation = Activations.IDENTITY;
		NeuralNetwork network = NeuralNetwork.build(2)
			.withLayer(2)
			.withBiases(new float[] { .5f, -.5f })
			.withWeights(new float[][] { { 0, 0 }, { 0, 0 } })
			.withActivationFunction(activation)
			.add()
			.build();

		INDArray inputs = Nd4j.create(new float[][] { { 1, 2 }, { 3, 4 } });
		INDArray outputs = network.evaluate(inputs);
		INDArray expectedOutputs = Nd4j.create(new float[][] { { .5f, -.5f }, { .5f, -.5f } });
		assertThat(outputs, array(expectedOutputs));

		network = NeuralNetwork.build(2)
			.withLayer(2)
			.withWeights(new float[][] { { .5f, 1f }, { 2f, 3f } })
			.withBiases(new float[] { 1f, -1f })
			.withActivationFunction(activation)
			.add()
			.build();
		outputs = network.evaluate(inputs);
		expectedOutputs = Nd4j.create(new float[][] { { 3.5f, 7f }, { 6.5f, 17f } });
		assertThat(outputs, array(expectedOutputs));

		network = NeuralNetwork.build(2)
			.withLayer(2)
			.withWeights(new float[][] { { .5f, 1f }, { 2f, 3f } })
			.withBiases(new float[] { 1f, -1f })
			.withActivationFunction(activation)
			.add()
			.withLayer(1)
			.withWeights(new float[][] { { 1f, 2f } })
			.withBiases(new float[] { 5f })
			.withActivationFunction(activation)
			.add()
			.build();

		outputs = network.evaluate(inputs);
		expectedOutputs = Nd4j.create(new float[][] { { 22.5f }, { 45.5f } });
		assertThat(outputs, array(expectedOutputs));

		activation = Activations.SIGMOID;
		network = NeuralNetwork.build(2)
			.withLayer(2)
			.withWeights(new float[][] { { .5f, 1f }, { 2f, 3f } })
			.withActivationFunction(Activations.IDENTITY)
			.add()
			.withLayer(1)
			.withWeights(new float[][] { { 1f, 2f } })
			.withActivationFunction(activation)
			.add()
			.build();

		outputs = network.evaluate(inputs);
		expectedOutputs = Nd4j.create(new float[][] { { apply(activation, 18.5f) }, { apply(activation, 41.5f) } });
		assertThat(outputs, array(expectedOutputs));
	}

	@Test
	public void testTrain() {
		// test from https://mattmazur.com/2015/03/17/a-step-by-step-backpropagation-example/
		NeuralNetwork network = NeuralNetwork.build(2)
			.withLayer(2)
			.withWeights(new float[][] { { .15f, .2f }, { .25f, .3f } })
			.withBiases(new float[] { .35f, .35f })
			.withActivationFunction(Activations.SIGMOID)
			.add()
			.withLayer(2)
			.withWeights(new float[][] { { .4f, .45f }, { .5f, .55f } })
			.withBiases(new float[] { .6f, .6f })
			.withActivationFunction(Activations.SIGMOID)
			.add()
			.build();

		INDArray inputs = Nd4j.create(new float[][] { { .05f, .1f } });
		INDArray expectedOutputs = Nd4j.create(new float[][] { { .01f, .99f } });

		assertThat(network.evaluate(inputs), array(Nd4j.create(new float[][] { { .75136507f, .772928465f } }), 1E-10f));
		
		network.train(inputs, expectedOutputs);

		// double totalError = LossFunction.SQUARED_LOSS.getILossFunction()
		// .computeScore(expectedOutputs, evaluated,
		// Activation.IDENTITY.getActivationFunction(), null, true);
		// System.out.println(totalError);
	}

	private float apply(ActivationFunction function, float value) {
		INDArray array = Nd4j.scalar(value);
		function.apply(array);
		return array.getFloat(0);
	}

	@Test
	public void testSerialize() throws IOException {
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.registerModule(new INDArrayModule());
		NeuralNetwork network = NeuralNetwork.build(4)
			.withLayer(8)
			.add()
			.withLayer(2)
			.withActivationFunction(Activations.SIGMOID)
			.add()
			.build();
		String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(network);
		System.out.println(json);
		NeuralNetwork nn = objectMapper.readValue(json, NeuralNetwork.class);
		assertEquals(network.layersCount(), nn.layersCount());
		assertEquals(network.inputsCount(), nn.inputsCount());
		for (int i = 0; i < network.layersCount(); i++) {
			assertLayerEquals(network.layer(i), nn.layer(i));
		}
	}

	private void assertLayerEquals(Layer expected, Layer actual) {
		assertEquals(expected.neurons(), actual.neurons());
		assertEquals(expected.inputs(), actual.inputs());
		for (int neuron = 0; neuron < expected.neurons(); neuron++) {
			for (int input = 0; input < expected.inputs(); input++) {
				assertEquals(expected.weight(neuron, input), actual.weight(neuron, input), 1E-6);
			}
			assertEquals(expected.bias(neuron), actual.bias(neuron), 1E-6);
		}
		assertSame(expected.activation(), actual.activation());
	}

	private static Matcher<INDArray> array(INDArray expected) {
		return array(expected, 1E-6);
	}

	private static Matcher<INDArray> array(INDArray expected, double eps) {
		Objects.requireNonNull(expected, "expected is null");
		return new BaseMatcher<INDArray>() {
			@Override
			public boolean matches(Object item) {
				if (item instanceof INDArray)
					return expected.equalsWithEps((INDArray) item, eps);
				return false;
			}

			@Override
			public void describeTo(Description description) {
				description.appendText(expected.toString());
			}
		};
	}

}
