package org.yah.tools.neuralnetwork.custom;

import java.util.ArrayList;
import java.util.List;

import org.deeplearning4j.datasets.iterator.FloatsDataSetIterator;
import org.nd4j.evaluation.classification.Evaluation;
import org.deeplearning4j.nn.api.OptimizationAlgorithm;
import org.deeplearning4j.nn.conf.MultiLayerConfiguration;
import org.deeplearning4j.nn.conf.NeuralNetConfiguration;
import org.deeplearning4j.nn.conf.layers.DenseLayer;
import org.deeplearning4j.nn.conf.layers.OutputLayer;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.nn.weights.WeightInit;
import org.nd4j.linalg.activations.Activation;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;
import org.nd4j.linalg.learning.config.Nesterovs;
import org.nd4j.linalg.lossfunctions.LossFunctions.LossFunction;
import org.nd4j.linalg.primitives.Pair;
import org.yah.tools.neuralnetwork.flowers.Flower;
import org.yah.tools.neuralnetwork.flowers.FlowerPredicate;
import org.yah.tools.neuralnetwork.flowers.Flowers;
import org.yah.tools.neuralnetwork.flowers.SimplexNoiseFlowerPredicate;
import org.yah.tools.neuralnetwork.gdxdemo.demos.FlowersNetworkFactory;

public class DL4JSandbox {

	public MultiLayerNetwork createNetwork(LossFunction lossFunction, int... layers) {
		NeuralNetConfiguration.ListBuilder builder = new NeuralNetConfiguration.Builder()
			.seed(123)
			.optimizationAlgo(OptimizationAlgorithm.STOCHASTIC_GRADIENT_DESCENT)
			.updater(new Nesterovs(.9))
			// .regularization(true).l2(0.0001)
			.weightInit(WeightInit.XAVIER)
			.list();

		int lastIndex = layers.length - 1;
		for (int i = 0; i < lastIndex - 1; i++) {
			int input = layers[i];
			int output = layers[i + 1];
			builder.layer(i, new DenseLayer.Builder()
				.nIn(input)
				.nOut(output)
				.activation(Activation.TANH)
				.build());
		}

		builder.layer(lastIndex - 1,
				new OutputLayer.Builder(lossFunction)
					.nIn(layers[lastIndex - 1])
					.nOut(layers[lastIndex])
					.activation(Activation.SIGMOID)
					.build());

		MultiLayerConfiguration conf = builder.build();
		MultiLayerNetwork network = new MultiLayerNetwork(conf);

		// network.addListeners(new ScoreIterationListener());

		network.init();
		return network;
	}

	public void testXor() {
		MultiLayerNetwork network = createNetwork(LossFunction.MSE, 2, 2, 1);

		float[][] inputs = new float[][] { { 0, 0 }, { 0, 1 }, { 1, 0 }, { 1, 1 } };
		float[][] outputs = new float[][] { { 0 }, { 1 }, { 1 }, { 0 } };
		DataSetIterator dataSet = createDatasSet(inputs, outputs);

		for (int i = 0; i < 15; i++) {
			network.fit(dataSet);
			System.out.println(network.evaluate(dataSet));
		}
	}

	public void testFlowers() {
		MultiLayerNetwork network = FlowersNetworkFactory.createNetwork();
		FlowerPredicate flowerPredicate = new SimplexNoiseFlowerPredicate();
		Flowers flowers = Flowers.random(flowerPredicate, 400);
		DataSetIterator dataSet = createDatasSet(flowers);
		for (int i = 0; i < 50; i++) {
			network.fit(dataSet);
			Evaluation evaluation = network.evaluate(dataSet);
			evaluation.setLabelsList(Flower.getNames());
			System.out.println(evaluation);
		}
	}

	private DataSetIterator createDatasSet(Flowers flowers) {
		List<Pair<float[], float[]>> pairs = new ArrayList<>(flowers.getCount());
		for (int i = 0; i < flowers.getCount(); i++) {
			float[] pos = flowers.getFlowerPosition(i);
			Flower flower = flowers.getFlower(i);
			pairs.add(new Pair<>(pos, flower.expectedOuputs()));
		}
		return new FloatsDataSetIterator(pairs, pairs.size());
	}

	private DataSetIterator createDatasSet(float[][] inputs, float[][] outputs) {
		assert inputs.length == outputs.length;
		List<Pair<float[], float[]>> pairs = new ArrayList<>(inputs.length);
		for (int i = 0; i < inputs.length; i++) {
			pairs.add(new Pair<>(inputs[i], outputs[i]));
		}
		return new FloatsDataSetIterator(pairs, pairs.size());
	}

	public static void main(String[] args) {
		new DL4JSandbox().testFlowers();
	}
}
