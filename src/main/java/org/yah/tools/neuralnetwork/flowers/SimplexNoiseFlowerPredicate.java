package org.yah.tools.neuralnetwork.flowers;

import org.yah.tools.neuralnetwork.flowers.noise.OpenSimplexNoise;
import static org.yah.tools.neuralnetwork.flowers.Flower.FLOWERS;

public class SimplexNoiseFlowerPredicate implements FlowerPredicate {

	private final OpenSimplexNoise noise;

	private final float noiseScale;

	public SimplexNoiseFlowerPredicate() {
		this(5f);
	}

	public SimplexNoiseFlowerPredicate(float noiseScale) {
		this(noiseScale, System.currentTimeMillis());
	}

	public SimplexNoiseFlowerPredicate(float noiseScale, long seed) {
		noise = new OpenSimplexNoise(seed);
		this.noiseScale = noiseScale;
	}

	@Override
	public Flower getFlower(double x, double y) {
		double n = noise.eval(x * noiseScale, y * noiseScale);
		n = (n + 1) * .5;
		double flowerIndex = n / (1f / FLOWERS.length);
		return FLOWERS[(int) flowerIndex];
	}

}
