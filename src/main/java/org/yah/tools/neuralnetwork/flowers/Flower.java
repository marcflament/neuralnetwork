package org.yah.tools.neuralnetwork.flowers;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import com.badlogic.gdx.graphics.Color;

public enum Flower {

	RED(Color.RED),
	GREEN(Color.GREEN);
	//,BLUE(Color.BLUE);

	public static Flower[] FLOWERS = Flower.values();

	public static List<String> NAMES = createNames();

	private final Color color;

	Flower(Color color) {
		this.color = color;
	}

	public Color getColor() {
		return color.cpy();
	}

	private static List<String> createNames() {
		return Collections.unmodifiableList(Arrays.stream(FLOWERS)
			.map(Flower::name)
			.collect(Collectors.toList()));
	}

	public float[] expectedOuputs() {
		float[] expectedOuputs = new float[FLOWERS.length];
		expectedOuputs[ordinal()] = 1;
		return expectedOuputs;
	}

	public static List<String> getNames() {
		return NAMES;
	}

	public static float[][] createFlowerOutputs() {
		float[][] res = new float[FLOWERS.length][];
		for (int i = 0; i < FLOWERS.length; i++) {
			res[i] = FLOWERS[i].expectedOuputs();
		}
		return res;
	}
}
