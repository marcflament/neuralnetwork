package org.yah.tools.neuralnetwork.flowers;

import java.util.Random;
import java.util.stream.StreamSupport;

public class Flowers {

	private final float[][] positions;

	private final Flower[] flowers;

	public Flowers(float[][] positions, Flower[] flowers) {
		this.positions = positions;
		this.flowers = flowers;
	}

	public int getCount() {
		return positions.length;
	}

	public Flower getFlower(int index) {
		return flowers[index];
	}

	public float[] getFlowerPosition(int index) {
		return positions[index];
	}

	public static Flowers from(Flowers from, Flower[] flowers) {
		return new Flowers(from.positions, flowers);
	}

	public static Flowers from(Flowers from, Iterable<Flower> flowers) {
		Flower[] flowersArray = StreamSupport.stream(flowers.spliterator(), false).toArray(Flower[]::new);
		return new Flowers(from.positions, flowersArray);
	}

	public static Flowers random(FlowerPredicate flowerPredicate, int count) {
		Random random = new Random();
		float[][] positions = new float[count][2];
		Flower[] colors = new Flower[count];
		for (int i = 0; i < count; i++) {
			positions[i][0] = random.nextFloat();
			positions[i][1] = random.nextFloat();
			colors[i] = flowerPredicate.getFlower(positions[i][0], positions[i][1]);
		}
		return new Flowers(positions, colors);
	}

	public static Flowers single(float x, float y) {
		return new Flowers(new float[][] { { x, y } }, new Flower[] { Flower.RED });
	}

	public float accuracy(Flowers matched) {
		int matchedCount = 0;
		for (int i = 0; i < flowers.length; i++) {
			if (flowers[i] == matched.flowers[i])
				matchedCount++;
		}
		return (float) matchedCount / flowers.length;
	}
}
