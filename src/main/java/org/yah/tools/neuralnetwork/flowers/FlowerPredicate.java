package org.yah.tools.neuralnetwork.flowers;

public interface FlowerPredicate {

	Flower getFlower(double x, double y);

}
