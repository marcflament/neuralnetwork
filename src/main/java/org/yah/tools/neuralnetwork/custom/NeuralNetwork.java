package org.yah.tools.neuralnetwork.custom;

import java.util.Arrays;
import java.util.EventListener;
import java.util.EventObject;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonAutoDetect(fieldVisibility = Visibility.ANY)
public class NeuralNetwork {

	public static class NetworkUpdateEvent extends EventObject {

		public NetworkUpdateEvent(NeuralNetwork source) {
			super(source);
		}

		@Override
		public NeuralNetwork getSource() {
			return (NeuralNetwork) super.getSource();
		}
	}

	public interface NetworkUpdateListener extends EventListener {
		void networkUpdated(NetworkUpdateEvent e);
	}

	@JsonAutoDetect(fieldVisibility = Visibility.ANY)
	public static final class Layer {
		private final INDArray weights; // matrix [neurons][inputs]
		private final INDArray biases; // matrix [neurons][1]
		private ActivationFunction activation;
		private float learningRate = .1f;

		@JsonCreator
		private Layer(@JsonProperty("weights") INDArray weights,
				@JsonProperty("biases") INDArray biases,
				@JsonProperty("activations") ActivationFunction activation) {
			assert weights.rows() == biases.rows();
			assert biases.columns() == 1;

			this.weights = weights;
			this.biases = biases;
			this.activation = activation;
		}

		public int neurons() {
			return weights.rows();
		}

		public int inputs() {
			return weights.columns();
		}

		public float weight(int neuron, int input) {
			return weights.getFloat(neuron, input);
		}

		public float bias(int neuron) {
			return biases.getFloat(neuron);
		}

		public ActivationFunction activation() {
			return activation;
		}

		private void setWeight(int neuron, int input, float weight) {
			weights.putScalar(neuron, input, weight);
		}

		private void setBias(int neuron, float bias) {
			biases.putScalar(neuron, bias);
		}

		private void setActivation(ActivationFunction activation) {
			this.activation = activation;
		}

		private INDArray propagateForward(INDArray layerInputs) {
			assert layerInputs.rows() == inputs();
			INDArray output = weights.mmul(layerInputs).addiColumnVector(biases);
			return activation.apply(output);
		}

		private void propagateBackward(INDArray dz, INDArray layerInputs) {
			int m = layerInputs.columns();
			INDArray dw = dz.mmul(layerInputs.transpose()).divi(m);
			INDArray db = dz.div(m);
			weights.subi(dw.mul(learningRate));
			biases.subi(db.mul(learningRate));
		}

		@Override
		public Layer clone() {
			INDArray newWeights = weights.dup();
			INDArray newBiases = biases.dup();
			return new Layer(newWeights, newBiases, activation);
		}

		@Override
		public String toString() {
			StringBuilder builder = new StringBuilder();
			builder.append("Layer [weights=")
				.append(Arrays.toString(weights.shape()))
				.append(", biases=")
				.append(Arrays.toString(biases.shape()))
				.append(", activation=")
				.append(Activations.getName(activation))
				.append("]");
			return builder.toString();
		}

	}

	private final Layer[] layers;

	@JsonIgnore
	private final List<NetworkUpdateListener> listeners = new LinkedList<>();

	@JsonCreator
	private NeuralNetwork(@JsonProperty("layers") Layer[] layers) {
		this.layers = layers;
	}

	public synchronized void addListener(NetworkUpdateListener listener) {
		this.listeners.add(listener);
	}

	public synchronized void removeListener(NetworkUpdateListener listener) {
		this.listeners.remove(listener);
	}

	/**
	 * Describe the network configuration giving neurons count for each layers
	 */
	public int[] layers() {
		int layersCount = layersCount();
		int[] res = new int[layersCount + 1];
		res[0] = inputsCount(0);
		for (int i = 0; i < layersCount; i++) {
			res[i + 1] = neurons(i);
		}
		return res;
	}

	public Layer layer(int index) {
		return layers[index];
	}

	public int layersCount() {
		return layers.length;
	}

	public int neurons(int layerIndex) {
		return layers[layerIndex].neurons();
	}

	public int inputsCount() {
		return inputsCount(0);
	}

	public int outputsCount() {
		return neurons(layers.length - 1);
	}

	public int inputsCount(int layer) {
		return layers[layer].inputs();
	}

	public float weight(int layer, int neuron, int input) {
		return layers[layer].weight(neuron, input);
	}

	public float bias(int layer, int neuron) {
		return layers[layer].bias(neuron);
	}

	public void setWeight(int layer, int neuron, int input, float weight) {
		layers[layer].setWeight(neuron, input, weight);
		fireUpdateEvent();
	}

	public void setBias(int layer, int neuron, float bias) {
		layers[layer].setBias(neuron, bias);
		fireUpdateEvent();
	}

	public void setActivation(int layer, ActivationFunction activation) {
		layers[layer].setActivation(activation);
		fireUpdateEvent();
	}

	public INDArray evaluate(INDArray inputs) {
		INDArray layerInputs = inputs.transpose();
		for (int layer = 0; layer < layers.length; layer++) {
			layerInputs = layers[layer].propagateForward(layerInputs);
		}
		return layerInputs.transpose();
	}

	public void train(INDArray inputs, INDArray expected) {
		INDArray transposedInputs = inputs.transpose();
		INDArray expectedOutputs = expected.transpose();

		// forward propagation, also store the outputs of each neurons
		INDArray layerInputs = transposedInputs;
		INDArray[] outputs = new INDArray[layers.length];
		for (int layer = 0; layer < layers.length; layer++) {
			outputs[layer] = layers[layer].propagateForward(layerInputs);
			layerInputs = outputs[layer];
		}

		// TODO : compute score

		// back propagation
		int lastLayer = layers.length - 1;
		for (int layer = lastLayer; layer >= 0; layer--) {
			INDArray layerOutputs = outputs[layer];

			// how much does the total error change with respect to the output
			INDArray errorOutput = null;
			if (layer == lastLayer) {
				// for last layer, only take error difference between current and expected
				errorOutput = outputs[layer].sub(expectedOutputs);
			} else {
				// TODO
			}
			System.out.println(errorOutput);
			// how much does the output change with respect to its total net input
			INDArray outputInput = outputs[layer].mul(outputs[layer].rsubi(1f));
			System.out.println(outputInput);
			// INDArray dz
		}

		// INDArray dz = outputs[lastLayer].sub(expectedOutputs);
		// layerInputs = outputs[layers.length - 2];
		// layers[lastLayer].propagateBackward(dz, layerInputs);
		// for (int layer = layers.length - 2; layer >= 0; layer--) {
		// if (layer > 0)
		// layerInputs = outputs[layer - 1];
		// else
		// layerInputs = transposedInputs;
		// dz = layers[layer +
		// 1].weights.transpose().mmuli(dz).mmuli(Transforms.pow(outputs[layer],
		// 2).rsub(1));
		// layers[layer].propagateBackward(dz, layerInputs);
		// }
	}

	@Override
	public String toString() {
		return Arrays.toString(layers());
	}

	@Override
	public NeuralNetwork clone() {
		Layer[] newLayers = new Layer[layers.length];
		for (int layer = 0; layer < newLayers.length; layer++) {
			newLayers[layer] = layers[layer].clone();
		}
		return new NeuralNetwork(layers);
	}

	private void fireUpdateEvent() {
		NetworkUpdateListener[] listenersSnapshot = listenersSnapshot();
		if (listenersSnapshot == null)
			return;

		NetworkUpdateEvent event = new NetworkUpdateEvent(this.clone());
		for (int i = 0; i < listenersSnapshot.length; i++) {
			listenersSnapshot[i].networkUpdated(event);
		}
	}

	private synchronized NetworkUpdateListener[] listenersSnapshot() {
		if (listeners.isEmpty())
			return null;
		return listeners.toArray(new NetworkUpdateListener[listeners.size()]);
	}

	public static Builder build(int inputs) {
		return new Builder(inputs);
	}

	public static class Builder {

		private final int inputs;

		private final LinkedList<Layer> layers = new LinkedList<>();

		public Builder(int inputs) {
			this.inputs = inputs;
		}

		public LayerBuilder withLayer(int size) {
			return new LayerBuilder(this, size);
		}

		public NeuralNetwork build() {
			if (layers.isEmpty())
				throw new IllegalStateException("at least one layer is required");
			Layer[] layersArray = layers.toArray(new Layer[layers.size()]);
			return new NeuralNetwork(layersArray);
		}
	}

	public static class Trainer {

	}

	public static class LayerBuilder {

		private final Builder builder;

		private final int neurons;

		private ActivationFunction activationFunction = Activations.RELU;

		private INDArray weights; // [neuron][input]
		private INDArray biases; // [neuron]

		private long randomSeed = new Random().nextLong();

		public LayerBuilder(Builder builder, int neurons) {
			this.builder = builder;
			this.neurons = neurons;
		}

		public LayerBuilder withActivationFunction(ActivationFunction activationFunction) {
			this.activationFunction = activationFunction;
			return this;
		}

		public LayerBuilder withWeights(INDArray array) {
			if (array.rows() != neurons)
				throw new IllegalArgumentException("weights count " + array.rows()
						+ " does not match neurons count " + neurons);
			this.weights = array;
			return this;
		}

		public LayerBuilder withWeights(float[][] weights) {
			return withWeights(Nd4j.create(weights));
		}

		public LayerBuilder withBiases(INDArray array) {
			if (array.columns() != 1)
				throw new IllegalArgumentException("Invalid bias array columns " + array.columns());
			if (array.rows() != neurons)
				throw new IllegalArgumentException("Invalid bias array rows " + array.rows()
						+ " does not match neurons "
						+ neurons);
			biases = array;
			return this;
		}

		public LayerBuilder withBiases(float[] bias) {
			return withBiases(Nd4j.create(bias, new int[] { bias.length, 1 }));
		}

		public LayerBuilder withRandomSeed(long randomSeed) {
			this.randomSeed = randomSeed;
			return this;
		}

		public Builder add() {
			if (weights == null) {
				weights = Nd4j.rand(neurons, inputs(), randomSeed);
			}

			if (biases == null) {
				biases = Nd4j.create(neurons, 1);
			}

			builder.layers.add(new Layer(weights, biases, activationFunction));
			return builder;
		}

		private int inputs() {
			return builder.layers.isEmpty() ? builder.inputs : builder.layers.getLast().neurons();
		}

	}

}
