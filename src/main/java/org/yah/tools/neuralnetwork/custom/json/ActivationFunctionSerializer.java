package org.yah.tools.neuralnetwork.custom.json;

import java.io.IOException;

import org.yah.tools.neuralnetwork.custom.ActivationFunction;
import org.yah.tools.neuralnetwork.custom.Activations;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

public class ActivationFunctionSerializer extends JsonSerializer<ActivationFunction> {

	@Override
	public void serialize(ActivationFunction value, JsonGenerator gen, SerializerProvider serializers)
			throws IOException {
		gen.writeString(Activations.getName(value));
	}

}
