package org.yah.tools.neuralnetwork.custom.json;

import java.io.IOException;

import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.core.TreeNode;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

public class INDArrayDeserializer extends JsonDeserializer<INDArray> {

	@Override
	public INDArray deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JsonProcessingException {
		TreeNode tree = p.readValueAsTree();
		if (tree instanceof ObjectNode) {
			ObjectNode objectNode = (ObjectNode) tree;
			int[] shape = readIntArray((ArrayNode) objectNode.get("shape"));
			double[] values = readDoubleArray((ArrayNode) objectNode.get("values"));
			return Nd4j.create(values, shape);
		}
		throw ctxt.wrongTokenException(p, INDArray.class, JsonToken.START_OBJECT, null);
	}

	private int[] readIntArray(ArrayNode node) {
		int[] res = new int[node.size()];
		for (int i = 0; i < res.length; i++) {
			res[i] = node.get(i).asInt();
		}
		return res;
	}

	private double[] readDoubleArray(ArrayNode node) {
		double[] res = new double[node.size()];
		for (int i = 0; i < res.length; i++) {
			res[i] = node.get(i).asDouble();
		}
		return res;
	}

}
