package org.yah.tools.neuralnetwork.custom;

import org.nd4j.linalg.api.ndarray.INDArray;

public interface CostFunction {

	float apply(INDArray actual, INDArray expected);
	
}
