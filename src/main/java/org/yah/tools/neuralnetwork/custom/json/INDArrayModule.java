package org.yah.tools.neuralnetwork.custom.json;

import org.nd4j.linalg.api.ndarray.INDArray;

import com.fasterxml.jackson.databind.module.SimpleModule;

public class INDArrayModule extends SimpleModule {

	public INDArrayModule() {
		addSerializer(INDArray.class, new INDArraySerializer());
		addDeserializer(INDArray.class, new INDArrayDeserializer());
	}

}
