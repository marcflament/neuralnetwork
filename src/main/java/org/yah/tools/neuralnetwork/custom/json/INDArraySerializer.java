package org.yah.tools.neuralnetwork.custom.json;

import java.io.IOException;

import org.nd4j.linalg.api.ndarray.INDArray;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

public class INDArraySerializer extends JsonSerializer<INDArray> {

	@Override
	public void serialize(INDArray value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
		gen.writeStartObject();
		long[] shape = value.shape();
		writeArrayField(gen, "shape", shape);
		writeArrayField(gen, "values", value.data().asDouble());
		gen.writeEndObject();
	}

	private void writeArrayField(JsonGenerator gen, String name, long[] values) throws IOException {
		gen.writeFieldName(name);
		gen.writeArray(values, 0, values.length);
	}

	private void writeArrayField(JsonGenerator gen, String name, double[] values) throws IOException {
		gen.writeFieldName(name);
		gen.writeArray(values, 0, values.length);
	}

}
