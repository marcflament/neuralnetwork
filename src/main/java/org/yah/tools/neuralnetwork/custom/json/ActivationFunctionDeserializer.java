package org.yah.tools.neuralnetwork.custom.json;

import java.io.IOException;

import org.yah.tools.neuralnetwork.custom.ActivationFunction;
import org.yah.tools.neuralnetwork.custom.Activations;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

public class ActivationFunctionDeserializer extends JsonDeserializer<ActivationFunction> {

	@Override
	public ActivationFunction deserialize(JsonParser p, DeserializationContext ctxt) throws IOException,
			JsonProcessingException {
		String text = p.getText();
		return Activations.getFunction(text);
	}

}
