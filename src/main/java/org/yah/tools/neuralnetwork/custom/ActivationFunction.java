package org.yah.tools.neuralnetwork.custom;

import java.util.function.Function;

import org.nd4j.linalg.api.ndarray.INDArray;
import org.yah.tools.neuralnetwork.custom.json.ActivationFunctionDeserializer;
import org.yah.tools.neuralnetwork.custom.json.ActivationFunctionSerializer;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize(using = ActivationFunctionSerializer.class)
@JsonDeserialize(using = ActivationFunctionDeserializer.class)
@FunctionalInterface
public interface ActivationFunction extends Function<INDArray, INDArray> {}