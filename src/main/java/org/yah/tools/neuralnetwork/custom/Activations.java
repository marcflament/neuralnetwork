package org.yah.tools.neuralnetwork.custom;

import org.nd4j.linalg.ops.transforms.Transforms;

public final class Activations {

	private Activations() {}

	public static final ActivationFunction SIGMOID = a -> Transforms.sigmoid(a, false);
	public static final ActivationFunction TANH = a -> Transforms.tanh(a, false);
	public static final ActivationFunction RELU = a -> Transforms.relu(a, false);
	public static final ActivationFunction LEAKY_RELU = a -> Transforms.leakyRelu(a, false);
	public static final ActivationFunction IDENTITY = a -> a;

	public static String getName(ActivationFunction function) {
		if (function == SIGMOID)
			return "SIGMOID";
		if (function == TANH)
			return "TANH";
		if (function == RELU)
			return "RELU";
		if (function == LEAKY_RELU)
			return "LEAKY_RELU";
		if (function == IDENTITY)
			return "IDENTITY";
		return function.getClass().getName();
	}

	public static ActivationFunction getFunction(String name) {
		switch (name) {
		case "SIGMOID":
			return SIGMOID;
		case "TANH":
			return TANH;
		case "RELU":
			return RELU;
		case "LEAKY_RELU":
			return LEAKY_RELU;
		case "IDENTITY":
			return IDENTITY;
		default:
			return fromClass(name);
		}
	}

	private static ActivationFunction fromClass(String name) {
		try {
			Class<?> cls = Class.forName(name);
			if (ActivationFunction.class.isAssignableFrom(cls))
				return (ActivationFunction) cls.newInstance();
			throw new IllegalArgumentException("Class " + cls.getName() + " is not an ActivationFunction");
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			throw new IllegalArgumentException("Unable to load ActivationFunction from name " + name, e);
		}
	}

}
