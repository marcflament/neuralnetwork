package org.yah.tools.neuralnetwork.custom;

import java.util.Iterator;
import java.util.NoSuchElementException;

import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;

public class LabeledResults<L> implements Iterable<L> {

	private final L[] labels;

	private final int[] indices;

	private final int size;

	public LabeledResults(float[][] outputs, L[] labels) {
		this(Nd4j.create(outputs), labels);
	}

	public LabeledResults(INDArray outputs, L[] labels) {
		indices = outputs.argMax(1).data().asInt();
		this.labels = labels;
		this.size = indices.length;
	}

	@Override
	public LabeledResultsIterator iterator() {
		return new LabeledResultsIterator();
	}

	public int size() {
		return size;
	}

	public class LabeledResultsIterator implements Iterator<L> {

		private int index;

		public int index() {
			return index;
		}

		@Override
		public boolean hasNext() {
			return index < size;
		}

		@Override
		public L next() {
			if (!hasNext())
				throw new NoSuchElementException();
			L res = labels[indices[index]];
			index++;
			return res;
		}

	}

}
