package org.yah.tools.neuralnetwork.data;

public class Images {
	private final int count;
	private final int width;
	private final int height;

	private final byte[] data;

	public Images(int count, int width, int height, byte[] data) {
		assert data.length == count * width * height;
		this.count = count;
		this.width = width;
		this.height = height;
		this.data = data;
	}

	public int getCount() {
		return count;
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

	public byte[] get(int index, byte[] target) {
		int imageSize = width * height;
		if (target == null)
			target = new byte[imageSize];
		System.arraycopy(data, index * imageSize, target, 0, imageSize);
		return target;
	}

	public String print(int index) {
		int imageSize = width * height;
		int offset = index * imageSize;
		StringBuilder sb = new StringBuilder();
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {
				int i = offset + y * width + x;
				int c = Byte.toUnsignedInt(data[i]);
				if (c > 100)
					sb.append('#');
				else
					sb.append(' ');
			}
			sb.append(System.lineSeparator());
		}
		return sb.toString();
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Images [count=")
			.append(count)
			.append(", width=")
			.append(width)
			.append(", height=")
			.append(height)
			.append("]");
		return builder.toString();
	}

	public int getImageSize() {
		return width * height;
	}

}