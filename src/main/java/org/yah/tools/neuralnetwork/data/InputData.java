package org.yah.tools.neuralnetwork.data;

import java.io.IOException;

public class InputData {
	private final String name;
	private final byte[] labels;
	private final Images images;

	private InputData(String name, byte[] labels, Images images) {
		this.name = name;
		if (labels.length != images.getCount())
			throw new IllegalStateException("Mismatched labels(" + labels.length + ") and images(" + images
				.getCount() + ") count");
		this.labels = labels;
		this.images = images;
	}

	public String getName() {
		return name;
	}

	public byte[] getLabels() {
		return labels;
	}

	public byte getLabel(int i) {
		return labels[i];
	}

	public Images getImages() {
		return images;
	}

	public byte[] get(int index, byte[] target) {
		return images.get(index, target);
	}

	public static InputData load(String name) throws IOException {
		byte[] labels = DataFiles.loadLabels("data/" + name + "-labels-idx1-ubyte.gz");
		Images images = DataFiles.loadImages("data/" + name + "-images-idx3-ubyte.gz");
		return new InputData(name, labels, images);
	}

	public static void main(String[] args) throws IOException {
		InputData id = load("t10k");
		int i = 45;
		System.out.println(id.getLabel(i));
		System.out.println(id.getImages().print(i));
	}

	public int getCount() {
		return labels.length;
	}

}