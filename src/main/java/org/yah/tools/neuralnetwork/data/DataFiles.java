package org.yah.tools.neuralnetwork.data;

import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.zip.GZIPInputStream;

public class DataFiles {

	private static final int LABELS_MAGIC_NUMBER = 0x00000801;
	private static final int IMAGES_MAGIC_NUMBER = 0x00000803;

	public static byte[] loadLabels(String file) throws IOException {
		try (GZIPInputStream zis = new GZIPInputStream(new FileInputStream(file))) {
			DataInputStream dais = new DataInputStream(zis);
			int magic = dais.readInt();
			if (magic != LABELS_MAGIC_NUMBER)
				throw new IOException("Invalid label magic number " + magic + " in " + file);

			int nb = dais.readInt();
			byte[] res = new byte[nb];
			dais.readFully(res);
			return res;
		}
	}

	public static Images loadImages(String file) throws IOException {
		try (GZIPInputStream zis = new GZIPInputStream(new FileInputStream(file))) {
			DataInputStream dais = new DataInputStream(zis);
			int magic = dais.readInt();
			if (magic != IMAGES_MAGIC_NUMBER)
				throw new IOException("Invalid images magic number " + magic + " in " + file);

			int count = dais.readInt();
			int height = dais.readInt();
			int width = dais.readInt();
			byte[] data = new byte[count * height * width];
			dais.readFully(data);
			return new Images(count, width, height, data);
		}
	}

}
