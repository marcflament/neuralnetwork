package org.yah.tools.neuralnetwork.gdxdemo.demos;

import static org.yah.tools.neuralnetwork.flowers.Flower.FLOWERS;

import org.yah.tools.neuralnetwork.flowers.Flower;
import org.yah.tools.neuralnetwork.gdxdemo.AbstractDemo;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;

public abstract class AbstractFlowersDemo extends AbstractDemo {

	private static final int HALF_FLOWER_WIDTH = 3;

	private static final int[] FLOWER_COLORS = new int[FLOWERS.length];
	private static final int[] UNMATCHED_FLOWER_COLORS = new int[FLOWERS.length];
	static {
		float f = .6f;
		for (int i = 0; i < FLOWERS.length; i++) {
			Color flowerColor = FLOWERS[i].getColor();
			FLOWER_COLORS[i] = Color.rgba8888(flowerColor);
			UNMATCHED_FLOWER_COLORS[i] = Color.rgba8888(flowerColor.cpy().mul(f));
		}
	}

	public static int flowerColor(Flower flower) {
		return FLOWER_COLORS[flower.ordinal()];
	}

	public static int unmatchedFlowerColor(Flower flower) {
		return UNMATCHED_FLOWER_COLORS[flower.ordinal()];
	}

	protected static void drawFlower(Pixmap buffer, int x, int y, int color) {
		buffer.setColor(color);
		buffer.drawLine(x - HALF_FLOWER_WIDTH, y - HALF_FLOWER_WIDTH, x + HALF_FLOWER_WIDTH, y + HALF_FLOWER_WIDTH);
		buffer.drawLine(x - HALF_FLOWER_WIDTH, y + HALF_FLOWER_WIDTH, x + HALF_FLOWER_WIDTH, y - HALF_FLOWER_WIDTH);
	}

}
