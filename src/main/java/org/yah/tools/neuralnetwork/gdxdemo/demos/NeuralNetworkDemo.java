package org.yah.tools.neuralnetwork.gdxdemo.demos;

import static org.yah.tools.neuralnetwork.flowers.Flower.FLOWERS;

import java.nio.IntBuffer;

import javax.swing.UIManager;

import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;
import org.yah.tools.neuralnetwork.custom.Activations;
import org.yah.tools.neuralnetwork.custom.LabeledResults;
import org.yah.tools.neuralnetwork.custom.NeuralNetwork;
import org.yah.tools.neuralnetwork.custom.NeuralNetwork.NetworkUpdateEvent;
import org.yah.tools.neuralnetwork.flowers.Flower;
import org.yah.tools.neuralnetwork.gdxdemo.DemoLauncher;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Pixmap;

public class NeuralNetworkDemo extends AbstractFlowersDemo {

	private NeuralNetwork network;

	private INDArray inputs;

	private NeuralNetworkConfigurationDialog dialog;

	private BackgroundExecutor backgroundExecutor;

	private void onNetworkChange(NetworkUpdateEvent e) {
		backgroundExecutor.submit(this::evaluate);
	}

	@Override
	public void create() {
		super.create();
		backgroundExecutor = new BackgroundExecutor();

		// @formatter:off
		network = NeuralNetwork.build(2)
			.withLayer(4)
				.withActivationFunction(Activations.TANH)
				.add()
			.withLayer(FLOWERS.length)
				.withActivationFunction(Activations.SIGMOID)
				.add()
			.build();
		// @formatter:on
		network.addListener(this::onNetworkChange);

		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
			e.printStackTrace();
		}
		dialog = new NeuralNetworkConfigurationDialog(network, null);
	}

	@Override
	public void dispose() {
		dialog.dispose();
		backgroundExecutor.shutdown();
		super.dispose();
	}

	@Override
	public void resize(int width, int height) {
		super.resize(width, height);
		inputs = null;
		backgroundExecutor.submit(this::evaluate);
	}

	private void evaluate() {
		if (inputs == null)
			inputs = createInputs(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		INDArray outputs = network.evaluate(inputs);
		schedule(p -> updateNetwork(outputs, p));
	}

	private void updateNetwork(INDArray outputs, Pixmap pixmap) {
		LabeledResults<Flower> results = new LabeledResults<>(outputs, FLOWERS);
		IntBuffer colorBuffer = toIntBuffer(pixmap);
		results.forEach(f -> colorBuffer.put(flowerColor(f)));
	}

	private INDArray createInputs(int width, int height) {
		int pixels = width * height;
		float[][] data = new float[pixels][2];
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {
				float dx = (x / (float) width) * 2f - 1;
				float dy = (1f - (y / (float) height)) * 2f - 1;
				float[] row = data[y * width + x];
				row[0] = dx;
				row[1] = dy;
			}
		}
		return Nd4j.create(data);
	}

	public static void main(String[] args) {
		DemoLauncher.launch(new NeuralNetworkDemo());
	}
}
