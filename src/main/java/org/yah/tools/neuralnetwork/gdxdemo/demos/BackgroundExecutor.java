package org.yah.tools.neuralnetwork.gdxdemo.demos;

import java.util.concurrent.CountDownLatch;

public class BackgroundExecutor {

	private boolean stopRequested;

	private Runnable nextTask;

	private final CountDownLatch exitLatch = new CountDownLatch(1);

	public BackgroundExecutor() {
		new Thread(this::loop).start();
	}

	private void loop() {
		try {
			while (!stopRequested()) {
				Runnable task = poll();
				task.run();
			}
		} catch (InterruptedException e) {
			// ignore
		} finally {
			exitLatch.countDown();
		}
	}

	public synchronized void submit(Runnable task) {
		nextTask = task;
		notify();
	}

	public void shutdown() {
		synchronized (this) {
			stopRequested = true;
			notify();
		}
		try {
			exitLatch.await();
		} catch (InterruptedException e) {}
	}

	private synchronized boolean stopRequested() {
		return stopRequested;
	}

	private synchronized Runnable poll() throws InterruptedException {
		while (nextTask == null && !stopRequested)
			wait();
		if (stopRequested)
			throw new InterruptedException();
		Runnable res = nextTask;
		nextTask = null;
		return res;
	}
}
