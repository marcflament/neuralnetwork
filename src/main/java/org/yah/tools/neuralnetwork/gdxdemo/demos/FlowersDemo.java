package org.yah.tools.neuralnetwork.gdxdemo.demos;

import org.yah.tools.neuralnetwork.flowers.Flower;
import org.yah.tools.neuralnetwork.flowers.FlowerPredicate;
import org.yah.tools.neuralnetwork.flowers.Flowers;
import org.yah.tools.neuralnetwork.flowers.SimplexNoiseFlowerPredicate;
import org.yah.tools.neuralnetwork.gdxdemo.DemoLauncher;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;

public class FlowersDemo extends AbstractFlowersDemo {

	protected FlowerPredicate flowersPredicate;

	protected Flowers flowers;

	protected DrawMode drawMode = DrawMode.FLOWERS;

	@Override
	public void create() {
		super.create();

		flowersPredicate = new SimplexNoiseFlowerPredicate(2, 56546);
		flowers = Flowers.random(flowersPredicate, 400);
	}

	@Override
	public void resize(int width, int height) {
		super.resize(width, height);
		schedule(this::redraw);
	}
	
	private void redraw(Pixmap buffer) {
		switch (drawMode) {
		case FLOWERS:
			drawFlowers(buffer);
			break;
		case SCREEN:
			drawNoise(buffer);
			break;
		}
	}

	private void drawFlowers(Pixmap buffer) {
		int width = buffer.getWidth();
		int height = buffer.getHeight();
		buffer.setColor(Color.BLACK);
		buffer.fill();
		for (int i = 0; i < flowers.getCount(); i++) {
			float[] pos = flowers.getFlowerPosition(i);
			Flower flower = flowers.getFlower(i);
			int x = (int) (pos[0] * width);
			int y = (int) (pos[1] * height);
			drawFlower(buffer, x, y, flowerColor(flower));
		}
	}

	protected void drawNoise(Pixmap buffer) {
		FlowerPredicateDemo.draw(flowersPredicate, buffer);
	}

	@Override
	public boolean keyDown(int keycode) {
		switch (keycode) {
		case Input.Keys.S:
			drawMode = DrawMode.SCREEN;
			schedule(this::redraw);
			return true;
		case Input.Keys.F:
			drawMode = DrawMode.FLOWERS;
			schedule(this::redraw);
			return true;
		default:
			return super.keyDown(keycode);
		}
	}

	public static void main(String[] args) {
		DemoLauncher.launch(new FlowersDemo());
	}
}
