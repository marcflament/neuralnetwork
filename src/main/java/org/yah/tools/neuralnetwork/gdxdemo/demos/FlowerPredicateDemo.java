package org.yah.tools.neuralnetwork.gdxdemo.demos;

import java.nio.IntBuffer;

import org.yah.tools.neuralnetwork.flowers.Flower;
import org.yah.tools.neuralnetwork.flowers.FlowerPredicate;
import org.yah.tools.neuralnetwork.flowers.SimplexNoiseFlowerPredicate;
import org.yah.tools.neuralnetwork.gdxdemo.DemoLauncher;

import com.badlogic.gdx.graphics.Pixmap;

public class FlowerPredicateDemo extends AbstractFlowersDemo {

	private FlowerPredicate flowerPredicate;

	private float scale = 5f;

	@Override
	public void create() {
		super.create();
		schedule(this::draw);
	}

	@Override
	public void resize(int width, int height) {
		super.resize(width, height);
		schedule(this::draw);
	}

	private void setScale(float scale) {
		this.scale = scale;
		flowerPredicate = null;
		schedule(this::draw);
	}

	private void draw(Pixmap buffer) {
		if (flowerPredicate == null)
			flowerPredicate = new SimplexNoiseFlowerPredicate(scale);
		draw(flowerPredicate, buffer);
	}

	static void draw(FlowerPredicate flowerPredicate, Pixmap buffer) {
		int w = buffer.getWidth();
		int h = buffer.getHeight();
		IntBuffer colorBuffer = toIntBuffer(buffer);
		for (int y = 0; y < h; y++) {
			for (int x = 0; x < w; x++) {
				Flower flower = flowerPredicate.getFlower(x / (float) w, y / (float) h);
				colorBuffer.put(flowerColor(flower));
			}
		}
	}

	@Override
	public boolean scrolled(int amount) {
		float newScale = Math.max(scale + amount, 1);
		setScale(newScale);
		return true;
	}

	public static void main(String[] args) {
		DemoLauncher.launch(new FlowerPredicateDemo());
	}
}
