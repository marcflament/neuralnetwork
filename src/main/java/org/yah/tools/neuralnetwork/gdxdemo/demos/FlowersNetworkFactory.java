package org.yah.tools.neuralnetwork.gdxdemo.demos;

import org.deeplearning4j.nn.api.OptimizationAlgorithm;
import org.deeplearning4j.nn.conf.MultiLayerConfiguration;
import org.deeplearning4j.nn.conf.NeuralNetConfiguration;
import org.deeplearning4j.nn.conf.layers.DenseLayer;
import org.deeplearning4j.nn.conf.layers.OutputLayer;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.nn.weights.WeightInit;
import org.nd4j.linalg.activations.Activation;
import org.nd4j.linalg.learning.config.Nesterovs;
import org.nd4j.linalg.lossfunctions.LossFunctions.LossFunction;
import org.yah.tools.neuralnetwork.flowers.Flower;

public class FlowersNetworkFactory {

	public static MultiLayerNetwork createNetwork() {
		int[] layers = new int[] { 2, 16, 8, Flower.FLOWERS.length };
		NeuralNetConfiguration.ListBuilder builder = new NeuralNetConfiguration.Builder()
			.seed(123)
			.optimizationAlgo(OptimizationAlgorithm.STOCHASTIC_GRADIENT_DESCENT)
			.updater(new Nesterovs(.9))
			.weightInit(WeightInit.NORMAL)
			.list();

		int lastIndex = layers.length - 1;
		for (int i = 0; i < lastIndex - 1; i++) {
			int input = layers[i];
			int output = layers[i + 1];
			builder.layer(i, new DenseLayer.Builder()
				.nIn(input)
				.nOut(output)
				.activation(Activation.TANH)
				.build());
		}

		builder.layer(lastIndex - 1,
				new OutputLayer.Builder(LossFunction.SQUARED_LOSS)
					.nIn(layers[lastIndex - 1])
					.nOut(layers[lastIndex])
					.activation(Activation.SIGMOID)
					.build());

		MultiLayerConfiguration conf = builder.build();
		MultiLayerNetwork network = new MultiLayerNetwork(conf);
		network.init();
		return network;
	}

}
