package org.yah.tools.neuralnetwork.gdxdemo.demos;

import static org.yah.tools.neuralnetwork.flowers.Flower.FLOWERS;

import java.util.Collections;
import java.util.Iterator;

import org.deeplearning4j.datasets.iterator.impl.ListDataSetIterator;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.factory.Nd4j;
import org.yah.tools.neuralnetwork.custom.LabeledResults;
import org.yah.tools.neuralnetwork.flowers.Flower;
import org.yah.tools.neuralnetwork.flowers.Flowers;
import org.yah.tools.neuralnetwork.flowers.SimplexNoiseFlowerPredicate;
import org.yah.tools.neuralnetwork.gdxdemo.DemoLauncher;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Align;

public class DL4JFlowersDemo extends AbstractFlowersDemo {

	private static final int ITERATIONS = 60;

	private static final int TRAINING_STEPS = 3;

	private MultiLayerNetwork network;

	private DataSet dataSet;

	private boolean training;

	private DrawMode drawMode = DrawMode.FLOWERS;

	private BackgroundExecutor backgroundExecutor;

	private BitmapFont font;

	private float accuracy;

	private SimplexNoiseFlowerPredicate flowersPredicate;

	private Flowers flowers;

	private LabeledResults<Flower> expectedScreenFlowers;

	private LabeledResults<Flower> predictedFlowers;

	private final int screenSamples = 400;

	private DataSet screenData;

	private DataSet flowersData;

	@Override
	public void create() {
		super.create();

		font = new BitmapFont();
		font.setColor(Color.WHITE);

		flowersPredicate = new SimplexNoiseFlowerPredicate(5f, 12345);
		flowers = Flowers.random(flowersPredicate, 400);
		backgroundExecutor = new BackgroundExecutor();
	}

	@Override
	public void resize(int width, int height) {
		super.resize(width, height);
		screenData = null;
		if (!training)
			backgroundExecutor.submit(this::evaluate);
	}

	@Override
	protected void render(SpriteBatch spriteBatch) {
		super.render(spriteBatch);
		int textWidth = 75;
		font.draw(spriteBatch, String.format("%.3f", accuracy),
				Gdx.graphics.getWidth() - textWidth - 10,
				Gdx.graphics.getHeight() - 10,
				textWidth, Align.right, false);
	}

	private void startTraining() {
		training = true;
		backgroundExecutor.submit(this::train);
	}

	private void train() {
		if (dataSet == null)
			dataSet = createDataSet();

		if (network == null)
			network = createNetwork();

		for (int s = 0; s < TRAINING_STEPS; s++) {
			DataSet trainingSet = trainingSet();
			if (trainingSet == null)
				break;
			for (int i = 0; i < ITERATIONS; i++) {
				network.fit(newDataSetIterator(trainingSet));
			}
		}

		evaluate();

		if (training)
			backgroundExecutor.submit(this::train);
	}

	private MultiLayerNetwork createNetwork() {
		return FlowersNetworkFactory.createNetwork();
	}

	private void evaluate() {
		if (dataSet == null)
			dataSet = createDataSet();

		if (network == null)
			network = createNetwork();

		INDArray output = network.output(newDataSetIterator(dataSet));
		predictedFlowers = new LabeledResults<>(output, FLOWERS);
		schedule(this::draw);
	}

	private ListDataSetIterator<DataSet> newDataSetIterator(DataSet dataSet) {
		return new ListDataSetIterator<>(Collections.singleton(dataSet), dataSet.numExamples());
	}

	private DataSet createDataSet() {
		DataSet res;
		switch (drawMode) {
		case SCREEN:
			res = createScreenData();
			expectedScreenFlowers = new LabeledResults<>(res.getLabels(), FLOWERS);
			break;
		case FLOWERS:
			res = createFlowersData();
			break;
		default:
			throw new UnsupportedOperationException("Unsupported draw mode " + drawMode);
		}
		return res;
	}

	private DataSet trainingSet() {
		switch (drawMode) {
		case FLOWERS:
			return dataSet;
		case SCREEN:
			return dataSet == null ? null : dataSet.sample(Math.min(dataSet.numExamples(), screenSamples));
		default:
			throw new UnsupportedOperationException("Unknown drawMode " + drawMode);
		}
	}

	private DataSet createScreenData() {
		if (screenData != null)
			return screenData;
		int width = Gdx.graphics.getWidth();
		int height = Gdx.graphics.getHeight();
		int size = width * height;
		INDArray inputs = Nd4j.create(size, 2);
		INDArray labels = Nd4j.zeros(size, FLOWERS.length);
		for (int y = 0; y < height; y++) {
			float dy = y / (float) height;
			for (int x = 0; x < width; x++) {
				float dx = x / (float) width;
				Flower flower = flowersPredicate.getFlower(dx, dy);
				int index = y * width + x;
				inputs.put(index, 0, dx);
				inputs.put(index, 1, dy);
				labels.put(index, flower.ordinal(), 1);
			}
		}
		screenData = new DataSet(inputs, labels);
		return screenData;
	}

	private DataSet createFlowersData() {
		if (flowersData != null)
			return flowersData;

		int flowersCount = flowers.getCount();
		INDArray inputs = Nd4j.create(flowersCount, 2);
		INDArray labels = Nd4j.create(flowersCount, FLOWERS.length);
		for (int i = 0; i < flowersCount; i++) {
			float[] flowerPosition = flowers.getFlowerPosition(i);
			Flower flower = flowers.getFlower(i);
			inputs.put(i, 0, flowerPosition[0]);
			inputs.put(i, 1, flowerPosition[1]);
			labels.put(i, flower.ordinal(), 1);
		}
		flowersData = new DataSet(inputs, labels);
		return flowersData;
	}

	@Override
	public void dispose() {
		training = false;
		backgroundExecutor.shutdown();
		font.dispose();
		super.dispose();
	}

	private void draw(Pixmap buffer) {
		buffer.setColor(Color.BLACK);
		buffer.fill();

		switch (drawMode) {
		case FLOWERS:
			drawFlowers(buffer);
			break;
		case SCREEN:
			drawScreen(buffer);
			break;
		default:
			break;
		}
	}

	private void drawScreen(Pixmap buffer) {
		int width = Gdx.graphics.getWidth();
		int height = Gdx.graphics.getHeight();
		int count = width * height;

		Iterator<Flower> actualIter = predictedFlowers != null ? predictedFlowers.iterator() : null;
		Iterator<Flower> expectedIter = expectedScreenFlowers != null ? expectedScreenFlowers.iterator() : null;
		int matched = 0;
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {
				Flower expected = expectedIter.next();
				Flower actual = actualIter != null ? actualIter.next() : null;

				int color;
				if (expected == actual) {
					matched++;
					color = flowerColor(expected);
				} else
					color = unmatchedFlowerColor(expected);

				buffer.drawPixel(x, y, color);
			}
		}
		accuracy = matched / (float) count;
	}

	private void drawFlowers(Pixmap buffer) {
		int count = flowers.getCount();
		int width = Gdx.graphics.getWidth();
		int height = Gdx.graphics.getHeight();
		int matched = 0;

		Iterator<Flower> actualIter = predictedFlowers != null ? predictedFlowers.iterator() : null;
		for (int i = 0; i < count; i++) {
			float[] pos = flowers.getFlowerPosition(i);
			int x = (int) (pos[0] * width);
			int y = (int) (pos[1] * height);

			Flower expected = flowers.getFlower(i);
			Flower actual = actualIter != null ? actualIter.next() : null;

			int color;
			if (expected == actual) {
				matched++;
				color = flowerColor(expected);
			} else
				color = unmatchedFlowerColor(expected);

			drawFlower(buffer, x, y, color);
		}
		accuracy = matched / (float) count;
	}

	@Override
	public boolean keyDown(int keycode) {
		switch (keycode) {
		case Input.Keys.S:
			setDrawMode(DrawMode.SCREEN);
			return true;
		case Input.Keys.F:
			setDrawMode(DrawMode.FLOWERS);
			return true;
		case Input.Keys.SPACE:
			if (training)
				training = false;
			else
				startTraining();
			return true;
		default:
			return super.keyDown(keycode);
		}
	}

	@Override
	public boolean keyUp(int keycode) {
		if (keycode == Input.Keys.SPACE)
			return true;
		return super.keyUp(keycode);
	}

	private void setDrawMode(DrawMode dm) {
		if (dm != drawMode) {
			drawMode = dm;
			dataSet = null;
			if (!training)
				backgroundExecutor.submit(this::evaluate);
		}
	}

	public static void main(String[] args) {
		DemoLauncher.launch(new DL4JFlowersDemo());
	}
}
