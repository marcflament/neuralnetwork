package org.yah.tools.neuralnetwork.gdxdemo.demos;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.SwingConstants;

import org.yah.tools.neuralnetwork.custom.NeuralNetwork;

public class NeuralNetworkConfigurationDialog {

	private final JDialog dialog;

	private static final Font TITLE_LABEL_FONT = Font.decode("Arial-BOLD-18");
	private static final Font SLIDER_LABEL_FONT = Font.decode("System-PLAIN-10");

	private final NeuralNetwork network;

	public NeuralNetworkConfigurationDialog(NeuralNetwork network, JFrame parent) {
		this.network = network;

		dialog = new JDialog(parent, "Controls", false);
		dialog.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
		dialog.getContentPane().add(createLayers());
		dialog.pack();
		if (parent != null) {
			Runnable positionUpdater = () -> dialog.setLocation(parent.getX() + parent.getWidth(), parent.getY());
			positionUpdater.run();
			parent.addComponentListener(new ComponentAdapter() {
				@Override
				public void componentMoved(ComponentEvent e) {
					positionUpdater.run();
				}

				@Override
				public void componentResized(ComponentEvent e) {
					positionUpdater.run();
				}
			});
		} else {
			dialog.setLocationByPlatform(true);
		}
		dialog.setVisible(true);
	}

	private Component createLayers() {
		Box box = Box.createHorizontalBox();
		box.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));

		int layerCount = network.layersCount();
		for (int i = 0; i < layerCount; i++) {
			box.add(createLayer(i));
			if (i < layerCount - 1)
				box.add(Box.createHorizontalStrut(5));
		}

		return box;
	}

	private Box createLayer(int layer) {
		Box box = Box.createVerticalBox();
		box.setAlignmentY(JComponent.TOP_ALIGNMENT);
		box.add(layerLabel(layer));

		box.add(Box.createVerticalStrut(5));

		int neurons = network.neurons(layer);
		for (int i = 0; i < neurons; i++) {
			box.add(createNeuron(layer, i));
			if (i < neurons - 1)
				box.add(Box.createVerticalStrut(5));
		}
		return box;
	}

	private Box createNeuron(int layer, int neuron) {
		Box box = Box.createVerticalBox();
		box.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY));

		box.add(inputsSlider("B" + neuron, biasUpdater(layer, neuron), network.bias(layer, neuron)));
		box.add(Box.createVerticalStrut(3));

		int inputs = network.inputsCount(layer);
		for (int i = 0; i < inputs; i++) {
			box.add(inputsSlider("W" + i, weightUpdater(layer, neuron, i), network.weight(layer, neuron, i)));
		}
		return box;
	}

	private Component layerLabel(int layer) {
		JLabel label = new JLabel("Layer " + layer);
		label.setFont(TITLE_LABEL_FONT);
		return label;
	}

	private JComponent inputsSlider(String label, FloatConsumer changeHandler, float value) {
		JPanel inputPanel = new JPanel();
		inputPanel.setLayout(new BoxLayout(inputPanel, BoxLayout.LINE_AXIS));

		JLabel titleLabel = new JLabel(label);
		titleLabel.setFont(SLIDER_LABEL_FONT);
		inputPanel.add(titleLabel);

		int min = -10, max = 10;
		JSlider slider = new JSlider(SwingConstants.HORIZONTAL, 0, 100, (int) (unlerp(min, max, value) * 100));
		inputPanel.add(slider);

		JLabel valueLabel = new JLabel();
		inputPanel.add(valueLabel);
		slider.addChangeListener(e -> {
			float v = lerp(min, max, slider.getValue() / 100f);
			valueLabel.setText(String.format("%.2f", v));
			changeHandler.consumer(v);
		});

		float v = lerp(min, max, slider.getValue() / 100f);
		valueLabel.setText(String.format("%.2f", v));

		return inputPanel;
	}

	private float unlerp(int min, int max, float f) {
		return (f - min) / (max - min);
	}

	private float lerp(int min, int max, float f) {
		return min + (max - min) * f;
	}

	private FloatConsumer biasUpdater(int layer, int neuron) {
		return b -> network.setBias(layer, neuron, b);
	}

	private FloatConsumer weightUpdater(int layer, int neuron, int input) {
		return w -> network.setWeight(layer, neuron, input, w);
	}

	public void dispose() {
		dialog.dispose();
	}

	@FunctionalInterface
	private interface FloatConsumer {
		void consumer(float f);
	}

}
